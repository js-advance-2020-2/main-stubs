const express = require("express");

const app = express();
const router = express.Router();

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


router.get("/getMainData", (req, res) => {
  setTimeout(() => {
    res.send(
      require('./getMainData')
    );
  }, 1000)
});

router.use('/static', express.static('./static'))

app.use(router)

const PORT = process.env.PORT;
const IP = process.env.IP;
  app.listen(PORT, () => {
    console.log(`listening on ${IP}:${PORT}/`)
  });
